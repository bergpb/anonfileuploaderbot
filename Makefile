NAME      := bergpb/anonfileuploaderbot
IMG       := ${NAME}:${TAG}
LATEST    := ${NAME}:latest
CONTAINER := $$(docker container ls -q --filter "name=afub")

build:
	@docker build -t ${IMG} .

tag:
	@docker tag ${IMG} ${LATEST}

format:
	@isort .
	@black .

deploy:
	@git pull
	@docker-compose up -d --no-deps --build

import hashlib
import os

import requests

from config import Config
from logger import logger

config = Config()

BOT_TOKEN = config.BOT_TOKEN


def process_file(file_id, file_size, bot_token):
    if not bool(file_size["success"]):
        logger.error(file_size)
        return file_size

    file_info = getting_file_info_from_telegram_api(file_id, bot_token)

    if not bool(file_info["success"]):
        logger.error(file_info)
        return file_info

    file_download = download_file_from_telegram_api(file_info, bot_token)

    if not bool(file_download["success"]):
        logger.error(file_download)
        return file_download

    file_links = upload_to_anonfile(file_info)

    if not bool(file_links["success"]):
        logger.error(file_links)

    return file_links


def getting_file_info_from_telegram_api(file_id, token):
    """Getting file information from Telegram API"""

    logger.debug(f"Getting file info from Telegram API: {file_id}")

    response = requests.get(
        f"https://api.telegram.org/bot{token}/getFile?file_id={file_id}"
    ).json()

    status = bool(response["ok"])

    if not status:
        logger.error("Failed to get file info from Telegram API")
        return {
            "success": False,
            "content": response["description"],
            "message": "Failed to get file info from Telegram API",
        }

    return {
        "success": True,
        "content": response["result"]["file_path"],
        "message": "Success get file info from Telegram API",
    }


def generate_hash_filename(filename):
    return hashlib.md5(filename.encode("utf-8")).hexdigest()


def download_file_from_telegram_api(file_info, bot_token):
    """Download file from Telegram API, if file is okay, return file content,
if not okay, return a error in json format"""

    logger.debug("Downloading file from Telegram API.")

    file = file_info["content"].split("/")[1]

    file_name = file.split(".")[0]
    file_extension = file.split(".")[1]

    logger.debug(f"Filename: {file_name}, File extension: {file_extension}")

    logger.debug("Generate hash from filename.")
    hash_filename = generate_hash_filename(file_name)

    logger.debug("Getting file from Telegram API.")
    file = requests.get(
        f"https://api.telegram.org/file/bot{bot_token}/{file_info['content']}"
    )

    try:
        res = file.json()
        status = bool(res["ok"])

        if not status:
            logger.error("Failed to get file info from Telegram API")
            return {
                "success": False,
                "content": res["description"],
                "message": "Failed to download content from Telegram API",
            }
    except Exception:
        logger.debug("Success to download content from Telegram API.")
        logger.debug("Creating file in tmp directory.")
        open(f"./tmp/{hash_filename}.{file_extension}", "wb").write(file.content)
        return {
            "success": True,
            "content": "Success",
            "message": "Success to download content from Telegram API.",
        }


def upload_to_anonfile(file_info):
    """Getting file and upload into AnonFiles"""

    file_name = file_info["content"].split("/")[1].split(".")[0]
    file_extension = file_info["content"].split(".")[1]

    logger.debug("Generate hash from filename.")
    hash_filename = generate_hash_filename(file_name)

    logger.debug("Uploading into AnonFile.")
    file = {"file": open(f"./tmp/{hash_filename}.{file_extension}", "rb")}
    res = requests.post("https://api.anonfiles.com/upload", files=file)

    try:
        status = bool(res.json()["status"])
        if not status:
            return {
                "success": False,
                "content": res.json()["message"],
                "message": "Fail to upload file into AnonFile.",
            }

        logger.debug(f"Success to upload into AnonFile.")

    except:
        logger.error(f"Error to upload file into AnonFile.")
        return {
            "success": False,
            "content": "API is not disponible.",
            "message": "Fail to upload file into AnonFile.",
        }

    logger.debug("Getting file links from AnonFile API.")
    links = res.json()["data"]["file"]["url"]
    logger.debug(f"Upload success!")

    logger.debug("Removing file from tmp folder.")
    os.remove(f"./tmp/{hash_filename}.{file_extension}")

    return {
        "success": True,
        "content": {"short_link": links["short"], "full_link": links["full"]},
        "message": "Success to get file links from AnonFile.",
    }


def get_file_size_in_mb(file_size, max_upload_size):
    file_size_in_mb = 0

    file_size_in_mb = file_size / 1024 / 1024

    if file_size_in_mb > max_upload_size:
        logger.error(f"File size is too big: {file_size_in_mb} MB")
        return {
            "success": False,
            "content": file_size_in_mb,
            "message": "File is too big. Only files with size less than 20MB can be downloaded from Telegram.",
        }

    logger.debug(f"File size is ok: {file_size_in_mb} MB")
    return {"success": True, "content": file_size_in_mb, "message": "File size is ok."}

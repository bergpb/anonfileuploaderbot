"""Helpers functions"""


def return_user_info(update):
    username = update.message.chat.username
    first_name = update.message.chat.first_name

    user_info = username if username is not None else first_name

    return user_info

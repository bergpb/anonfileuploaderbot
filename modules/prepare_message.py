"""Prepare messagens to send to user"""


def initial_msg(user):
    return f"""🤖*AnonFileUploaderBot*🤖\n
Hello {user},
Send me some file to upload to AnonFile.
I returning a link to you share your file with some friends.
Avaliable commands:
/start - Start bot,
/help - Send help information.
❗️ *Only files with size equal our less than 20MB can be downloaded into Telegram.*"""


def help_msg():
    return f"""🤖*AnonFileUploaderBot*🤖\n
ℹ️ Send me a file to upload to AnonFile and get a link to download.
We don't save your files in server and your file name has changed before upload to AnonFile.
Check AnonFile terms here: [https://anonfile.com/terms](https://anonfile.com/terms)
❗️ *Only files with size equal our less than 20MB can be downloaded into Telegram.*"""


def command_not_found_message():
    return """🤖*AnonFileUploaderBot*🤖\n
Ops! Command not found.
Try to send me /start to start bot,
/help to send help information,
or just send me a file to upload into AnonFile."""


def mount_upload_msg(content):
    return f"""🤖*AnonFileUploaderBot*🤖\n
✅ Your file has been uploaded into AnonFile:
🔗 Short Link: [{content["content"]['short_link']}]({content["content"]['short_link']})
🔗 Full Link: [{content["content"]['full_link']}]({content["content"]['full_link']})"""


def error_message(content):
    return f"""🤖*AnonFileUploaderBot*🤖\n
❗️ *Ops! Something is wrong:*
❗️ Message: {content["message"]}"""


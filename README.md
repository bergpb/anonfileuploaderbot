# AnonFileUploaderBot

### AnonFileUpload site suspended your services, and this bot can´t work without the API, this repo will be archived.

This bot upload file into AnonFile and return a link to download.

Requirements: `docker` and `docker-compose`.

### How to run:

  1. Clone project,
  1. Enter in project folder,
  1. Create a ```.env``` based in ```.env.example``` with command ```cp -n .env.example .env``` changing with your variables,
  1. Run command ```docker-compose -f docker-compose-dev.yml up``` to build services and start services.


### ToDo

  1. [x] Create a Dockerfile,
  1. [x] Use `docker-compose` for dev and prod environments,
  1. [ ] Implement tests,
  1. [x] Better logging,
  1. [ ] Implementing metrics without save any user data,
  1. [x] Using queue to work with uploads,
  1. [x] Better error logging with queue.


### Some git commands:

  ##### Managing branchs:
  1. List all branchs: `git branch -a`,
  1. Creating and checkout a new branch: `git checkout -b dev`,
  1. Sending modifications into new branch: `git push origin branch`.
  ##### Managing tags:
  1. Creating a new tag: `git tag -a vx.x.x -m "version x.x.x"`,
  1. Pushing a new tag into repo: `git push origin vx.x.x`.
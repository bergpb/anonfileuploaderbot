# AnonfileUploaderBot

Telegram bot with capabilities to upload a attachment to [https://anonfile.com](https://anonfile.com)

Requirements: `kubernetes`, `kubectl` and `kustomize`

### Kubernetes dev environment:

  1. Apply the manifests file: `kustomize build overlays/<dev|prod> | kubectl apply -f -`

### Kubernetes prod environment:

  1. Replace BOT_TOKEN value with bot token generated with [@botfather](https://t.me/botfather) in `overlays/<dev|prod>/.env` file
  2. Apply the manifests file: `kustomize build overlays/<dev|prod> | kubectl apply -f -`

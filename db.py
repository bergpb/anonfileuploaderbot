# http://pythonclub.com.br/gerenciando-banco-dados-sqlite3-python-parte1.html
# http://pythonclub.com.br/gerenciando-banco-dados-sqlite3-python-parte2.html

import sqlite3


class Database(object):

    tb_name = "jobs"

    def __init__(self):
        try:
            self.conn = sqlite3.connect(
                "anonfileuploaderbot.db", check_same_thread=False
            )
            self.cursor = self.conn.cursor()
        except sqlite3.Error:
            return False

    def create_tables(self):
        self.cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS jobs (
                id         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                job_id     TEXT NOT NULL,
                chat_id    TEXT NOT NULL,
                created_in TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
            );
        """
        )

    def insert_job(self, job_id, chat_id):
        self.cursor.execute(
            """
            INSERT INTO jobs (job_id, chat_id) VALUES (?, ?)""",
            (
                job_id,
                chat_id,
            ),
        )
        self.commit_db()

    def select_all_jobs(self):
        res = self.cursor.execute("""SELECT * FROM jobs;""")
        return res.fetchall()

    def remove_job(self, job_id):
        self.cursor.execute("""DELETE FROM jobs WHERE job_id = ?""", (job_id,))
        self.commit_db()

    def drop_table(self):
        self.cursor.execute("""DROP TABLE IF EXISTS jobs""")
        self.commit_db()

    def close_connection(self):
        self.close_db()

    def commit_db(self):
        if self.conn:
            self.conn.commit()

    def close_db(self):
        if self.conn:
            self.conn.close()


if __name__ == "__main__":
    db = Database()
    db.drop_table()
    db.create_tables()

from pathlib import Path

from loguru import logger

from config import Config

# logger.debug("debug message")
# logger.info("info message")
# logger.warning("warn message")
# logger.error("error message")
# logger.critical("critical message")

config = Config()

formatter = (
    "[{level.icon}{level:^10}] {time:YYYY-MM-DD hh:mm:ss} {file} - {name}: {message}"
)

try:
    level = config.LOGGER_LEVEL
except AttributeError:
    level = "INFO"
try:
    file_name = config.LOGGER_FILE
except AttributeError:
    file_name = "anonfileuploaderbot.log"

log_path = Path(".") / file_name

logger.add(log_path, format=formatter, level=level, rotation="100 MB", colorize=True)

FROM python:alpine3.12 AS base
LABEL maintainer=bergpb

FROM base AS builder

RUN apk update && \
    apk add --no-cache \
    gcc \
    musl-dev \
    libffi-dev \
    openssl-dev \
    python3-dev

COPY requirements.txt /tmp

RUN pip install --upgrade pip && \
    pip install --prefix=/install \
     -r /tmp/requirements.txt --no-warn-script-location

FROM base

RUN mkdir /home/anonfileuploaderbot

WORKDIR /home/anonfileuploaderbot

RUN mkdir /home/anonfileuploaderbot/tmp

COPY --from=builder /install /usr/local

COPY . /home/anonfileuploaderbot

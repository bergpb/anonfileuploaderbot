import sys
import traceback
from datetime import datetime as dt

from functools import wraps

from redis import Redis
from rq import Queue
from rq.job import Job
from telegram import ParseMode, Update, ChatAction
from telegram.ext import (
    CallbackContext,
    CommandHandler,
    Filters,
    MessageHandler,
    Updater,
)

from config import Config
from db import Database
from logger import logger
from modules import file_operations, helpers, prepare_message

config = Config()
db = Database()

ENV = config.ENV
MAX_UPLOAD_SIZE_IN_MB = 20
BOT_TOKEN = config.BOT_TOKEN
DEV_CHAT_ID = config.DEV_CHAT_ID
REDIS_URL = config.REDIS_URL

q = Queue(connection=Redis.from_url(REDIS_URL))


def send_typing_action(f):
    """Sends typing action while processing func command."""

    @wraps(f)
    def command_func(update, context, *args, **kwargs):
        context.bot.send_chat_action(
            chat_id=update.effective_message.chat_id, action=ChatAction.TYPING
        )
        return f(update, context, *args, **kwargs)

    return command_func


def logging_user_info(f):
    """Logger user info"""

    @wraps(f)
    def command_func(update, context, *args, **kwargs):
        logger.info(
            f"Username: {helpers.return_user_info(update)}, Command: {update.message.text}, Datetime: {update.message.date}"
        )
        return f(update, context, *args, **kwargs)

    return command_func


def send_file_received(f):
    """Sends file received to user."""

    @wraps(f)
    def command_func(update, context, *args, **kwargs):
        update.message.reply_text(
            """🤖*AnonFileUploaderBot*🤖\n
Hey, we receive your file, we will check some things and if everything is okay upload your file.""",
            parse_mode="Markdown",
        )
        return f(update, context, *args, **kwargs)

    return command_func


@send_typing_action
@logging_user_info
def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""

    user = helpers.return_user_info(update)

    content = prepare_message.initial_msg(user)

    update.message.reply_text(content, parse_mode=ParseMode.MARKDOWN)


@send_typing_action
def help(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""

    content = prepare_message.help_msg()

    update.message.reply_text(content, parse_mode=ParseMode.MARKDOWN)


@send_typing_action
@send_file_received
@logging_user_info
def photo_received(update: Update, context: CallbackContext) -> None:
    """Photo received from chat"""

    chat_id = update["message"]["chat"]["id"]

    logger.debug("Photo received...")

    last_item = len(update["message"]["photo"]) - 1
    file_id = update["message"]["photo"][last_item]["file_id"]

    file_size = file_operations.get_file_size_in_mb(
        update["message"]["photo"][last_item]["file_size"], MAX_UPLOAD_SIZE_IN_MB
    )

    job = q.enqueue(
        file_operations.process_file, file_id, file_size, BOT_TOKEN, result_ttl=300
    )

    db.insert_job(job.id, chat_id)


@send_typing_action
@send_file_received
@logging_user_info
def document_received(update: Update, context: CallbackContext) -> None:
    """Document received from chat"""

    chat_id = update["message"]["chat"]["id"]

    logger.debug("Document received...")

    file_id = update["message"]["document"]["file_id"]

    file_size = file_operations.get_file_size_in_mb(
        update["message"]["document"]["file_size"], MAX_UPLOAD_SIZE_IN_MB
    )

    job = q.enqueue(
        file_operations.process_file, file_id, file_size, BOT_TOKEN, result_ttl=300
    )

    db.insert_job(job.id, chat_id)


@send_typing_action
@send_file_received
@logging_user_info
def audio_received(update: Update, context: CallbackContext) -> None:
    """Audio received from chat"""

    chat_id = update["message"]["chat"]["id"]

    logger.debug("Audio received...")

    file_id = update["message"]["audio"]["file_id"]

    file_size = file_operations.get_file_size_in_mb(
        update["message"]["audio"]["file_size"], MAX_UPLOAD_SIZE_IN_MB
    )

    job = q.enqueue(
        file_operations.process_file, file_id, file_size, BOT_TOKEN, result_ttl=300
    )

    db.insert_job(job.id, chat_id)


@send_typing_action
@send_file_received
@logging_user_info
def video_received(update: Update, context: CallbackContext) -> None:
    """Video received from chat"""

    chat_id = update["message"]["chat"]["id"]

    logger.debug("Video received...")

    file_id = update["message"]["video"]["file_id"]

    file_size = file_operations.get_file_size_in_mb(
        update["message"]["video"]["file_size"], MAX_UPLOAD_SIZE_IN_MB
    )

    job = q.enqueue(
        file_operations.process_file, file_id, file_size, BOT_TOKEN, result_ttl=300
    )

    db.insert_job(job.id, chat_id)


@send_typing_action
@send_file_received
@logging_user_info
def voice_received(update: Update, context: CallbackContext) -> None:
    """Voice received from chat"""

    chat_id = update["message"]["chat"]["id"]

    logger.debug("Voice received...")

    file_id = update["message"]["voice"]["file_id"]

    file_size = file_operations.get_file_size_in_mb(
        update["message"]["voice"]["file_size"], MAX_UPLOAD_SIZE_IN_MB
    )

    job = q.enqueue(
        file_operations.process_file, file_id, file_size, BOT_TOKEN, result_ttl=300
    )

    db.insert_job(job.id, chat_id)


@send_typing_action
@logging_user_info
def not_found_command(update: Update, context: CallbackContext):
    """Send a message if command is not recognized."""

    content = prepare_message.command_not_found_message()

    update.message.reply_text(content, parse_mode=ParseMode.MARKDOWN)


def error(update: Update, context: CallbackContext):
    """Log all errors"""

    trace_error = "".join(traceback.format_tb(sys.exc_info()[2]))

    logger.warning(
        f"""Context: {context.error},
Update: {update},
Trace error: {trace_error}"""
    )

    update.message.reply_text(
        "❗️ Something is wrong, please try again latter.", parse_mode=ParseMode.MARKDOWN
    )


def check_jobs(context: CallbackContext):
    all_jobs = db.select_all_jobs()

    for job in all_jobs:
        job_id = job[1]
        job_info = Job.fetch(job_id, connection=Redis.from_url(REDIS_URL))
        if job_info.get_status() == "finished":
            content = job_info.result
            chat_id = job[2]
            if content["success"] == True:
                content = prepare_message.mount_upload_msg(content)
            else:
                content = prepare_message.error_message(content)
            context.bot.send_message(chat_id, content, ParseMode.MARKDOWN)
            db.remove_job(job_id)


def main():
    updater = Updater(BOT_TOKEN, use_context=True)

    dispatcher = updater.dispatcher

    job = updater.job_queue
    job.run_repeating(check_jobs, interval=5)

    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help))

    dispatcher.add_handler(MessageHandler(Filters.photo, photo_received))
    dispatcher.add_handler(MessageHandler(Filters.document, document_received))
    dispatcher.add_handler(MessageHandler(Filters.audio, audio_received))
    dispatcher.add_handler(MessageHandler(Filters.video, video_received))
    dispatcher.add_handler(MessageHandler(Filters.voice, voice_received))

    dispatcher.add_handler(MessageHandler(Filters.command, not_found_command))

    dispatcher.add_handler(MessageHandler(Filters.text, not_found_command))

    dispatcher.add_error_handler(error)

    updater.start_polling()

    updater.idle()


if __name__ == "__main__":
    db.drop_table()
    db.create_tables()
    logger.debug("Bot started ...")
    main()

import os
from pathlib import Path

from dotenv import load_dotenv

env_path = Path(".") / ".env"
load_dotenv(dotenv_path=env_path)


class Config:
    ENV = os.getenv("ENV", "development")

    BOT_TOKEN = os.getenv("BOT_TOKEN", "")

    DEV_CHAT_ID = os.getenv("DEV_CHAT_ID", "")

    REDIS_URL = os.getenv("REDIS_URL", "redis://localhost:6379")

    LOGGER_LEVEL = os.getenv("LOGGER_LEVEL", "DEBUG")
    LOGGER_FILE = os.getenv("LOGGER_FILE", "logger.log")
